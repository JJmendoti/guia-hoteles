
$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
        interval: 2500
    });

    $('#contactar').on('show.bs.modal', function (e){
        console.log("el modal se esta mostrando");

        $('#contactoBtn').removeClass('btn btn-info');
        $('#contactoBtn').addClass('btn btn-warning');
        $('#contactoBtn').prop('disabled', true);
        //btn2
        $('#contactoBtn2').removeClass('btn btn-info');
        $('#contactoBtn2').addClass('btn btn-warning');
        $('#contactoBtn2').prop('disabled', true);
        //btn3
        $('#contactoBtn3').removeClass('btn btn-info');
        $('#contactoBtn3').addClass('btn btn-warning');
        $('#contactoBtn3').prop('disabled', true);
        //btn4
        $('#contactoBtn4').removeClass('btn btn-info');
        $('#contactoBtn4').addClass('btn btn-warning');
        $('#contactoBtn4').prop('disabled', true);
        //btn5
        $('#contactoBtn5').removeClass('btn btn-info');
        $('#contactoBtn5').addClass('btn btn-warning');
        $('#contactoBtn5').prop('disabled', true);
        //btn6
        $('#contactoBtn6').removeClass('btn btn-info');
        $('#contactoBtn6').addClass('btn btn-warning');
        $('#contactoBtn6').prop('disabled', true);
    });


    $('#contactar').on('shown.bs.modal', function (e){
        console.log("el modal contacto se mostró");
    });
    
    $('#contactar').on('hide.bs.modal', function (e){
        console.log("el modal contacto se oculta");
        
    });

    $('#contactar').on('hidden.bs.modal', function (e){
        console.log("el modal contacto se ocultó");
        $('#contactoBtn').prop('disabled', false);
        $('#contactoBtn').removeClass('btn btn-warning')
        $('#contactoBtn').addClass('btn btn-info');
        //btn2
        $('#contactoBtn2').prop('disabled', false);
        $('#contactoBtn2').removeClass('btn btn-warning')
        $('#contactoBtn2').addClass('btn btn-info');

        //btn3
        $('#contactoBtn3').prop('disabled', false);
        $('#contactoBtn3').removeClass('btn btn-warning')
        $('#contactoBtn3').addClass('btn btn-info');

         //btn4
         $('#contactoBtn4').prop('disabled', false);
         $('#contactoBtn4').removeClass('btn btn-warning')
         $('#contactoBtn4').addClass('btn btn-info');

          //btn5
        $('#contactoBtn5').prop('disabled', false);
        $('#contactoBtn5').removeClass('btn btn-warning')
        $('#contactoBtn5').addClass('btn btn-info');
         //btn6
         $('#contactoBtn6').prop('disabled', false);
         $('#contactoBtn6').removeClass('btn btn-warning')
         $('#contactoBtn6').addClass('btn btn-info');
    });
});